名称： 帮你妹（哈哈）
类型： 微信小程序
语言： JavaScript
作者： 绿色的小苹果（Jack Li）

微信小程序的推出，无疑将会在移动互联网行业里再次掀起风浪。
有人会质疑小程序会不会火， 会不会火我不知道， 看微信的用户量即可明白一切。

此小程序集成众多功能（天气查询， 手机号查询， 身份证查询， 历史上的今天， 美女图片（哈哈， 你懂的）等）。 目前只完成天气查询， 历史上的今天， 美女图片（重要）， 剩余功能会陆续完善， 也希望有兴趣的小伙伴（妹子优先）共同完成。 当然， 由于小苹果作者非ps出身， 实在做不出好看的素材， 所以显得比较简朴， 希望能在大家的共同努力下做出炫酷的“帮你妹”。

目前使用接口：
天气（心知天气）：https://api.thinkpage.cn/v3/weather/daily.json?key=your_api_key&location=beijing&language=zh-Hans&unit=c&start=0&days=5
历史今天：http://www.ipip5.com/today/api.php
美女（天行数据）：http://api.tianapi.com/meinv/?key=APIKEY&num=10

【接口调试
    common下新建const.js文件

    // 百度地图key
    module.exports = {
    mapKey: "your key",
    mapUrl: "http://api.map.baidu.com/geocoder/v2/?",

    // 天气（心知天气）
    weatherKey: "your key",
    weatherUrl: "https://api.thinkpage.cn/v3/weather/daily.json",

    // 天行数据（美女图片）
    tianxingKey: "your key",
    girlNum: "20" // 显示的数量
}
】

小程序交流： QQ群（214975625）  作者微信：（737692621）